package cn.dam.zip.kin2.zipk2.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @ClassName ZipKin2Controller
 * @Description TODO
 * @Author dam
 * @Date 2018/11/27 9:46 AM
 * Version 1.0
 **/
@RestController
@Slf4j
public class ZipKin2Controller {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping(value = "/zip2")
    public String hi(){

        return restTemplate.getForObject("http://localhost:8770/ziphome",String.class);
    }

    @RequestMapping(value = "/home")
    public String home(){
        return "hi i am zip2";
    }

}
