package cn.dam.zip.kin2.zipk2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class Zipk2Application {

  public static void main(String[] args) {
    SpringApplication.run(Zipk2Application.class, args);
  }
  @Bean
  RestTemplate restTemplate(){
    return new RestTemplate();
  }

}
