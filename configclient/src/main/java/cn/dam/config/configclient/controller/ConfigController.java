package cn.dam.config.configclient.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName ConfigController
 * @Description TODO
 * @Author dam
 * @Date 2018/11/22 4:29 PM
 * Version 1.0
 **/
@RestController
@Slf4j
public class ConfigController {

    @Value("${foo}")
    private String foo;

    @GetMapping(value = "/getFoo")
    public String getFoo(){
        return  foo;
    }



}
