package cn.dam.sp.cloud.eukacli.eucl.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName ReclController2
 * @Description TODO
 * @Author dam
 * @Date 2018/11/20 6:05 PM
 * Version 1.0
 **/

@RestController
@RequestMapping("/ecl")
public class ReclController2 {


    @GetMapping(value = "/getRecl/{name}")
    public String getRecl(@PathVariable String name){
        return "hi,i am from recl2 "+name;
    }

    @GetMapping("/hi")
    public String hi(){
        return "hi";
    }

}
