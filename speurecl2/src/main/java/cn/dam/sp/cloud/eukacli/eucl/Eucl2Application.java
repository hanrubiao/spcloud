package cn.dam.sp.cloud.eukacli.eucl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class Eucl2Application {

  public static void main(String[] args) {
    SpringApplication.run(Eucl2Application.class, args);
  }
}
