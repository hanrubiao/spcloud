package cn.dam.zip.kin.zipki.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @ClassName ZipKinController
 * @Description TODO
 * @Author dam
 * @Date 2018/11/26 6:49 PM
 * Version 1.0
 **/
@RestController
@Slf4j
public class ZipKinController {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping(value = "/zip")
    public String hi(){
        return restTemplate.getForObject("http://localhost:8771/home",String.class);
    }

    @RequestMapping(value = "/ziphome")
    public String home(){
        log.info("hi,this is zip");
        return  "i am zip";
    }

}
