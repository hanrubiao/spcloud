package cn.dam.speureka.speu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class SpeuApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpeuApplication.class, args);
  }
}
