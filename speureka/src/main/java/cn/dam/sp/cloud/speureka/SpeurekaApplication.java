package cn.dam.sp.cloud.speureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class SpeurekaApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpeurekaApplication.class, args);
  }
}
