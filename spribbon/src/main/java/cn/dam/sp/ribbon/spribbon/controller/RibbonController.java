package cn.dam.sp.ribbon.spribbon.controller;

import cn.dam.sp.ribbon.spribbon.service.RibbonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName RibbonController
 * @Description TODO
 * @Author dam
 * @Date 2018/11/20 5:51 PM
 * Version 1.0
 **/

@RestController
@RequestMapping(value = "/ribbo")
@Slf4j
public class RibbonController {

    @Autowired
    private RibbonService ribbonService ;

    @GetMapping(value = "/gtRib")
    public String getRibbon(@RequestParam  String name){
        log.info("name:"+name);
        return  ribbonService.ribblonRest(name);
    }

    @GetMapping(value = "/hi")
    public String hi(){
        return "hi";
    }


}
