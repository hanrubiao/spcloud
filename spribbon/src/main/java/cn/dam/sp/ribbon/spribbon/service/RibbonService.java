package cn.dam.sp.ribbon.spribbon.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @ClassName RibbonService
 * @Description TODO
 * @Author dam
 * @Date 2018/11/20 5:52 PM
 * Version 1.0
 **/

@Service
@Slf4j
public class RibbonService {

    @Autowired
    private  RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "riberror")
    public String ribblonRest(String name){
        log.info("name:"+name);
        return  restTemplate.getForObject
                ("http://eurecl/ecl/getRecl/"+name,String.class);
    }

    public String riberror(String name){
        return "error "+name;
    }
}
