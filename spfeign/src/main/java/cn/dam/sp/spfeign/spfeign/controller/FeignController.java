package cn.dam.sp.spfeign.spfeign.controller;

import cn.dam.sp.spfeign.spfeign.service.FeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName FeignController
 * @Description TODO
 * @Author dam
 * @Date 2018/11/21 2:43 PM
 * Version 1.0
 **/
@RestController
public class FeignController {

    @Autowired
    private FeignService feignService;

    @GetMapping(value = "/getFeign")
    public String getFeign(@RequestParam String name){
        return  feignService.feignStr(name);
    }

    @GetMapping(value = "/hi")
    public String hi(){
        return "hi";
    }


}
