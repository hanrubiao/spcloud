package cn.dam.sp.spfeign.spfeign.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @ClassName FeignService
 * @Description TODO
 * @Author dam
 * @Date 2018/11/21 2:45 PM
 * Version 1.0
 **/

@FeignClient(value = "eurecl")
public interface FeignService {

    @GetMapping(value = "/ecl/getRecl/{name}")
    String feignStr(@PathVariable(value = "name") String name);
}
